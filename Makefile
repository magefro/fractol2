# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/09/08 18:34:12 by alangloi          #+#    #+#              #
#    Updated: 2021/09/21 18:28:57 by alangloi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

UNAME_S := $(shell uname -s)

ifeq ($(UNAME_S),Linux)
	SYSTEM = linux/
	IMPORT_MLX = -lXext -lX11 -lm -lbsd
else
	SYSTEM =
	IMPORT_MLX = -Lmlx -lmlx -framework OpenGL -framework AppKit
endif

NAME = fractol
SOURCES = gcc
FLAGS = -Wall -Werror -Wextra
LIBFT = libft/libft.a
MLX = mlx/libmlx.a
HEADER = includes
SRC = sources
OBJ = objects
SOURCES = fractol.c \
		output.c \
		zoom.c \
		draw.c \
		init.c \
		controls.c \
		utils.c
SRCS = $(addprefix $(SRC)/, $(SOURCES))
OBJS = $(addprefix $(OBJ)/, $(SOURCES:.c=.o))

all: $(OBJ) $(NAME)

$(OBJ):
	mkdir -p $(OBJ)

$(NAME): $(OBJS)
	make -C $(SYSTEM)mlx
	make -C libft
	$(CC) $(FLAGS) -o $(NAME) $(OBJS) $(LIBFT) $(SYSTEM)$(MLX) $(IMPORT_MLX)

$(OBJ)/%.o: $(SRC)/%.c
	$(CC) $(FLAGS) -o $@ -c $^ -I$(HEADER) -Imlx -O3 -g

clean:
	make clean -C libft
	make clean -C $(SYSTEM)mlx
	rm -rf $(OBJ)

fclean: clean
	make fclean -C libft
	rm -rf $(NAME)

re: fclean all

.PHONY: all fclean clean re
