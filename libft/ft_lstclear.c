/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/17 00:07:54 by alangloi          #+#    #+#             */
/*   Updated: 2019/12/18 20:58:29 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstclear(t_list **lst, void (*del)(void *))
{
	t_list		*elem;
	t_list		*next;

	if (!lst && !del)
		return ;
	elem = *lst;
	while (elem)
	{
		next = elem->next;
		del(elem->content);
		free(elem);
		elem = elem->next;
	}
	*lst = NULL;
}
