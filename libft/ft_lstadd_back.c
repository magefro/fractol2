/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_back.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/17 00:05:27 by alangloi          #+#    #+#             */
/*   Updated: 2020/01/02 15:04:16 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_back(t_list **alst, t_list *new)
{
	t_list		*elem;

	if (!new)
		return ;
	new->next = NULL;
	if (!*alst)
	{
		ft_lstadd_front(alst, new);
	}
	if (alst)
	{
		elem = ft_lstlast(*alst);
		elem->next = new;
		elem->next->next = NULL;
	}
}
