/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/05 22:54:10 by alangloi          #+#    #+#             */
/*   Updated: 2019/12/12 23:08:52 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s1, char const *set)
{
	size_t		end;
	size_t		start;
	char		*str;

	if (!s1 || !set)
		return (NULL);
	start = 0;
	while (s1[start] != '\0')
	{
		if (ft_strchr(set, s1[start]) == NULL)
			break ;
		start++;
	}
	if (s1[start] == '\0')
		return (ft_strdup(""));
	end = ft_strlen(s1);
	while (end)
	{
		if (ft_strchr(set, s1[end - 1]) == NULL)
			break ;
		end--;
	}
	end = end - start;
	str = ft_substr((char *)s1, start, end);
	return (str);
}
