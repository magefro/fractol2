/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/06 19:06:40 by alangloi          #+#    #+#             */
/*   Updated: 2021/09/21 18:25:54 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

void	init_program(t_fract *f)
{
	f->pixel.rr = 100 % (0x4F + 0x01);
	f->pixel.rg = 100 % (0x4F + 0x01);
	f->pixel.rb = 0 % (0x4F + 0x01);
	f->w = 800;
	f->h = 800;
	f->zoom = 100;
	f->mlx_ptr = mlx_init();
	f->win_ptr = mlx_new_window(f->mlx_ptr, f->w, f->h, "fractol");
	f->img_ptr = mlx_new_image(f->mlx_ptr, f->w, f->h);
	f->img_data = (unsigned char *)mlx_get_data_addr
		(f->img_ptr, &f->bpp, &f->size_line, &f->endian);
}

static void	print_julia_help(void)
{
	ft_putstr_fd("Wrong arguments\t./fractol julia <float1> <float2>\n", 1);
	ft_putstr_fd("\n", 1);
	ft_putstr_fd("Here are some examples of nice Julia fractals :\n", 1);
	ft_putstr_fd("\t-0.7269 0.1889\n", 1);
	ft_putstr_fd("\t-0.8 0.156\n", 1);
	ft_putstr_fd("\t-0.835 -0.2321\n", 1);
	ft_putstr_fd("\t-0.70176 -0.3842\n", 1);
	ft_putstr_fd("\t0.45 0.1428\n", 1);
	ft_putstr_fd("\t0.285 0.01\n", 1);
	ft_putstr_fd("\t0.285 0\n", 1);
}

static void	setup_x(t_fract *f)
{
	if (f->id == 1 || f->id == 2)
	{
		f->xmin = -2.0;
		f->xmax = 2.0;
	}
	else if (f->id == 3)
	{
		f->xmin = -2.5;
		f->xmax = 1;
	}
}

void	init_fract(t_fract *f)
{
	f->maxiter = 100;
	setup_x(f);
	f->ymin = -2.0;
	f->ymax = f->ymin + (f->xmax - f->xmin) * f->h / f->w;
	if (f->id == 2)
	{
		if (f->argc == 4 && str_digit(f->argv[2]) && str_digit(f->argv[3]))
		{
			f->j.r = ft_atoi_double(f->argv[2]);
			f->j.i = ft_atoi_double(f->argv[3]);
		}
		else
		{
			print_julia_help();
			free(f);
			exit(0);
		}
	}
}
