/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/06 19:06:16 by alangloi          #+#    #+#             */
/*   Updated: 2021/09/08 21:28:19 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

/*
 * setup the variables of the differents fractals
 */

static void	setup_fract(t_fract *f)
{
	if (f->id == 1 || f->id == 3)
	{
		f->z.r = 0.0;
		f->z.i = 0.0;
	}
	if (f->id == 2)
	{
		f->z.i = f->ymin + f->y * f->yscale;
		f->z.r = f->xmin + f->x * f->xscale;
		f->c.r = f->j.r;
		f->c.i = f->j.i;
	}
}

/*
 * launching the algorithm of the fractal
 */

void	fractal_set(t_fract *f)
{
	int		i;
	double	tmp;

	setup_fract(f);
	i = 0;
	while (((f->z.r * f->z.r) + (f->z.i * f->z.i) <= 4.0) && (i < f->maxiter))
	{
		tmp = (f->z.r * f->z.r) - (f->z.i * f->z.i) + f->c.r;
		if (f->id == 3)
			f->z.i = fabs(2 * f->z.r * f->z.i) + f->c.i;
		else
			f->z.i = 2 * f->z.r * f->z.i + f->c.i;
		f->z.r = tmp;
		i++;
	}
	colors(f, i);
}

/*
 * displaying the fractal throught an x y canvas
 */

void	display_fractal(t_fract *f)
{
	f->xscale = (f->xmax - f->xmin) / (f->w - 1);
	f->yscale = (f->ymax - f->ymin) / (f->h - 1);
	f->y = 0;
	while (f->y < f->h)
	{
		f->c.i = f->ymin + f->y * f->yscale;
		f->x = 0;
		while (f->x < f->w)
		{
			f->c.r = f->xmin + f->x * f->xscale;
			fractal_set(f);
			print_pixel(f);
			f->x++;
		}
		f->y++;
	}
	mlx_put_image_to_window(f->mlx_ptr, f->win_ptr, f->img_ptr, 0, 0);
}
