/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/06 19:07:43 by alangloi          #+#    #+#             */
/*   Updated: 2021/09/21 15:47:57 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# if defined (__APPLE__)

#  include <mlx.h>
#  define ARROW_UP		125
#  define ARROW_DOWN	126
#  define ARROW_LEFT	123
#  define ARROW_RIGHT	124
#  define COLORKEY		8
#  define ESCKEY		53

# elif defined (__linux__)

#  include "../linux/mlx/mlx.h"
#  define ARROW_UP		65364
#  define ARROW_DOWN	65362
#  define ARROW_LEFT	65361
#  define ARROW_RIGHT	65363
#  define COLORKEY		99
#  define ESCKEY		65307

# endif

# include "../libft/libft.h"
# include <stdlib.h>
# include <stdio.h>
# include <math.h>
# include <limits.h>

typedef struct s_complex
{
	double		r;
	double		i;
}				t_complex;

typedef struct s_color
{
	int		r;
	int		g;
	int		b;
	int		rr;
	int		rb;
	int		rg;
}				t_color;

typedef struct s_fract
{
	int				argc;
	char			**argv;
	int				id;
	void			*mlx_ptr;
	void			*win_ptr;
	void			*img_ptr;
	unsigned char	*img_data;
	int				bpp;
	int				size_line;
	int				endian;
	int				h;
	int				w;
	int				x;
	int				y;
	t_color			pixel;
	t_complex		c;
	t_complex		z;
	t_complex		j;
	double			xmin;
	double			xmax;
	double			ymin;
	double			ymax;
	double			xscale;
	double			yscale;
	int				maxiter;
	double			zoom;
}				t_fract;

/* draw */
void	display_fractal(t_fract *f);
void	fractal_set(t_fract *f);

/* zoom */
int		mouse_hook(int button, int x, int y, t_fract *f);

/* key controls */
int		key_hook(int keycode, t_fract *f);
int		close_window(int keycode, t_fract *f);

/* init */
void	init_program(t_fract *f);
void	init_fract(t_fract *f);

/* output */
void	print_pixel(t_fract *f);
void	colors(t_fract *f, int i);

/* utils */
int		str_digit(char *new);

#endif
